import pathlib
from setuptools import setup

HERE = pathlib.Path(__file__).parent
REQUIREMENTS = (HERE / "requirements.txt").read_text().splitlines()


if __name__ == "__main__":
    setup(
        name='assistant',
        version='0.2.0.dev1',
        packages=[
            'assistant',
            'assistant.grpc_server',
            'assistant.interface_algorithm',
            'assistant.interface_algorithm.language_data'
            ],
        package_data={
            'assistant': [
                'interface_algorithm/language_data/model_get_type_21.12.pkl',
                'interface_algorithm/language_data/model_getQA_14.12.pkl',
                'interface_algorithm/language_data/model3.pkl',
            ],
        },
        include_package_data=True,
        install_requires=REQUIREMENTS,
        python_requires='==3.8.10',
    )
