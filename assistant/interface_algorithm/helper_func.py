import os
from appdirs import user_config_dir, user_data_dir
from assistant import __path__ as package_path


class Helpers:
    def __init__(self, data_path = None) -> None:
        self.assistant_path = package_path[0]
        if data_path:
            self.data_path = data_path
            self.config_path = data_path
        else:
            self.data_path = user_data_dir("assistant", "kallkod")
            self.config_path = user_config_dir("assistant", "kallkod")
        
    def package_path(self):
        """Returns path to the root package directory"""
        return self.assistant_path

    def model_path(self, model):
        """Returns path to model, located in language_data directory"""
        models_path = os.path.join(self.assistant_path, 'interface_algorithm/language_data/', model)
        return models_path

    def user_data_path(self):
        r"""Returns path to user_data directory.
        Typical locations should be:
        Unix:       ~/.local/share/assistant
        Win 7:      C:\Users\<OS user name>\AppData\Local\kallkod\assistant
        """
        return os.path.join(self.data_path, "user_data")

    def user_config_path(self):
        r"""Returns path to user_data directory.
        Typical locations should be:
        Unix:       ~/.config/assistant
        Win 7:      C:\Users\<OS user name>\AppData\Local\kallkod\assistant
        """
        return self.config_path

    def user_id_path(self):
        """Returns path to user_id file"""
        path_to_user_id = os.path.join(self.data_path, "/user_id")
        return path_to_user_id
