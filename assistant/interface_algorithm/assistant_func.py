#!/usr/bin/env python3
# coding: utf-8

# assistant functions

import os

import pandas as pd

from assistant.interface_algorithm import algorithm_func
from assistant.interface_algorithm.helper_func import Helpers


# Assistant_func class declaration
class Assistant_func():
    def __init__(self, h: Helpers, a : algorithm_func.Algorithm_func) -> None:
        self.user_id_path = h.user_id_path()
        self.a = a

    def isuser(self, data_folder, user_id):
        """Chesk if user exist"""
        users = self.account_list(data_folder)
        return user_id in users

    def account_list(self, data_folder):
        """Get contents of data_folder as a list, remove from that list everything that is not a directory.
        Return list of subdirectories located in data_folder."""

        directory_contents = os.listdir(data_folder)
        
        dir_list = [item for item in directory_contents if os.path.isdir(os.path.join(data_folder, item))]

        return dir_list

    def user_existance_check(self, data_folder, user_id):
        """Check if the user directory exists. If it doesn't exist, return empty string.
        user_id :: name of the user account folder.
        data_folder :: path to the user_data directory"""

        user_exist = self.isuser(data_folder, user_id)  #True/False

        dir_name = ''

        if user_id and user_exist:
            dir_name = os.path.join(data_folder, user_id)

        return dir_name  # full path to user account folder

    def create_user(self, data_folder, new_user_id):
        """Create directory named from new_user_id value inside data_folder. 
        Return string, path to that newly created directory"""

        dir_name = os.path.join(data_folder, new_user_id)
        os.mkdir(dir_name)

        return dir_name

    def get_username(self):
        """get username from user_id file"""
        """Read user_id file and get the name of active user. Returns sting value."""
        with open (self.user_id_path) as user_id_file:
            username = (user_id_file.read()).strip(' \n\t\r')
        return username

    def write_username(self, username):
        """Write name of the active user to user_id file. Doesn't return anything"""
        with open(self.user_id_path, "w") as user_id_file:
            user_id_file.write(username)

    def subject2data(self, dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline):  
        
        # Checking if the list empty
        if subject == []: 
            subject = ''
        
        if type(subject) == str:
            result = self.a.get_access(dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline)
            
        if type(subject) == list:
            result = self.a.create_empty_data()
            for i in subject:
                out = self.a.get_access(dir_name, message, message_id, todo, i, start_time, end_time, is_deadline)
                result = result.append(out)
            
        if self.a.isempty(result):
            message_id = 'log'
            if type(subject) == str:
                result = self.a.get_access(dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline)
            
            if type(subject) == list:
                result = self.a.create_empty_data()
                for i in subject:
                    out = self.a.get_access(dir_name, message, message_id, todo, i, start_time, end_time, is_deadline)
                    result = result.append(out)
        
        #removed dublicate row from the search
        result = result[~result.index.duplicated(keep='first')]
            
        return result
    
    def sort_result_by(self, result, column_name, asc):  
        result = result.sort_values(column_name, ascending=asc)
        return result
    
    def your_message(self, message, message_id, deadline):
        colnames = ["message_id", "message", "deadline"]
        data_temp = pd.DataFrame([[message_id, message, deadline]], columns=colnames)
        return data_temp
