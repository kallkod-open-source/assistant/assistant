#!/usr/bin/env python3
# coding: utf-8

import datetime
from calendar import monthrange

sec = datetime.timedelta(seconds=1);

# Algorithm_time_func class declaration
class Algorithm_time_func():
    def str2daytime(self, str_time):
        return datetime.datetime.strptime(str_time, '%Y-%m-%d')

    def today(self, today = datetime.datetime.now()):
        begin = today.replace(hour=0, minute=0, second=0, microsecond=0)
        end   = begin + datetime.timedelta(days=1) - sec
        return str(today), str(end)

    def tomorrow(self, today = datetime.datetime.now()):
        today = today.replace(hour=0, minute=0, second=0, microsecond=0)
        begin = today + datetime.timedelta(days=1)
        end   = begin + datetime.timedelta(days=1)  - sec
        return str(begin), str(end)

    def yesterday(self, today = datetime.datetime.now()):
        today = today.replace(hour=0, minute=0, second=0, microsecond=0)
        start = today - datetime.timedelta(days=1)
        end   = today  - sec
        return str(start), str(end)

    def current_week(self, today = datetime.datetime.now()):
        today_init  = today.replace(hour=0, minute=0, second=0, microsecond=0)
        monday      = today_init - datetime.timedelta(days = today_init.weekday())
        next_monday = monday + datetime.timedelta(days=7)
        return str(today), str(next_monday - sec)

    def next_week(self, today = datetime.datetime.now()):
        today       = today.replace(hour=0, minute=0, second=0, microsecond=0)
        monday      = today + datetime.timedelta(days =- today.weekday(), weeks=1)
        next_monday = monday + datetime.timedelta(days=7)
        return str(monday), str(next_monday - sec)

    def last_week(self, today = datetime.datetime.now()):
        today       = today.replace(hour=0, minute=0, second=0, microsecond=0)
        monday      = today - datetime.timedelta(days=today.weekday(), weeks=1)
        next_monday = monday + datetime.timedelta(days=7)
        return str(monday), str(next_monday - sec)

    def current_month(self, today = datetime.datetime.now()):
        today_init    = today.replace(hour=0, minute=0, second=0, microsecond=0)
        days_in_month = lambda today: monthrange(today_init.year, today_init.month)[1]
        last_day      = today_init.replace(day=1) + datetime.timedelta(days_in_month(today_init))
        return str(today), str(last_day - sec)

    def next_month(self, today = datetime.datetime.now()):
        today              = today.replace(hour=0, minute=0, second=0, microsecond=0)
        days_in_cur_month  = lambda today: monthrange(today.year, today.month)[1]
        first_day          = today.replace(day=1) + datetime.timedelta(days_in_cur_month(today))
        days_in_next_month = lambda first_day: monthrange(first_day.year, first_day.month)[1]
        last_day = first_day.replace(day=1) + datetime.timedelta(days_in_next_month(first_day))
        return str(first_day), str(last_day - sec)

    def last_month(self, today = datetime.datetime.now()): 
        today               = today.replace(hour=0, minute=0, second=0, microsecond=0)
        first_day_cur_month = today.replace(day=1)
        last_day            = first_day_cur_month - datetime.timedelta(days=1)
        first_day           = last_day.replace(day=1)
        return str(first_day), str(first_day_cur_month - sec)

    def current_year(self, today = datetime.datetime.now()):    
        first_day = today
        cur_year  = today.year
        last_day  = datetime.datetime(cur_year + 1, 1, 1, 0, 0)
        return str(first_day), str(last_day - sec)

    def last_year(self, today = datetime.datetime.now()):    
        cur_year  = today.year
        first_day = datetime.datetime(cur_year - 1, 1, 1, 0, 0)
        last_day  = datetime.datetime(cur_year, 1, 1, 0, 0)
        return str(first_day), str(last_day - sec)

    def next_year(self, today = datetime.datetime.now()):    
        cur_year  = today.year
        first_day = datetime.datetime(cur_year + 1, 1, 1, 0, 0)
        last_day  = datetime.datetime(cur_year + 2, 1, 1, 0, 0)
        return str(first_day), str(last_day - sec)
