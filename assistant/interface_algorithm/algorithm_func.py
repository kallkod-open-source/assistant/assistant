#!/usr/bin/env python3
# coding: utf-8

import os
import os.path
import shutil
import datetime
import pathlib
import re
import pandas as pd
import numpy as np
from assistant.interface_algorithm import algorithm_language_func
from assistant.interface_algorithm import algorithm_time_func
from assistant.interface_algorithm import helper_func
from assistant.message_type import MessageType


t = algorithm_time_func.Algorithm_time_func()


pd.options.mode.chained_assignment = None  # default='warn'

custom_key_word = 'sum'

# Algorithm_func class declaration
class Algorithm_func():
    def __init__(self, helper: helper_func.Helpers, lang: algorithm_language_func.Algorithm_language_func) -> None:
        path = pathlib.Path(helper.user_data_path())
        path.mkdir(parents=True, exist_ok=True)
        self.lang = lang

    def isempty(self,data):
        """bolean function is dataframe empty"""
        result = data.empty
        return result

    def rm_dirr(self,dir_path):
        """remove folder recursively"""
        try:
            shutil.rmtree(dir_path)
        except OSError as e:
            print("Error: %s : %s" % (dir_path, e.strerror))

    def id_except(self, message_id):
        if message_id != 'log' and message_id != 'time' and message_id != 'location':
            raise Exception('The message must be log, time or location. The message id was: {}'.format(message_id))

    def date_except(self, col_name):
        if col_name != 'date' and col_name != 'deadline':
            raise Exception('Column name must be date or deadline. Column name was: {}'.format(col_name))

    def create_empty_data(self):
        colnames = ["date","message_id", "message", "deadline"]
        data = pd.DataFrame(columns=colnames)
        return data

    def read_data(self, dir_name, message_id):
        self.id_except(message_id)
        
        filename = os.path.join(dir_name, 'data_'+message_id+'_assistant.csv')
        
        if not os.path.isfile(filename):
            colnames = ["date","message_id", "message", "deadline"]
            data = pd.DataFrame(columns=colnames)
            data.to_csv(filename, index = False)
        
        return pd.read_csv(filename)

    def write_data(self, dir_name, id_message, message, deadline = '', drop_all = False, drop_time = True, drop_loc = True):
    
        # if data folder does not exist, create it
        if os.path.isdir(dir_name):
            pass
        else:
            os.mkdir(dir_name)
    
        # if dataframe does not exist, create it
        current_datetime = pd.to_datetime('now')
        def data_(message_id = 'log'):
            try:
                data = self.read_data(dir_name, message_id)
            except IOError:
                colnames = ["date","message_id", "message", "deadline"]
                data = pd.DataFrame([], columns=colnames)
    
            colnames = ["date","message_id", "message", "deadline"]
            data_new_line = pd.DataFrame([[current_datetime, id_message, message, deadline]], columns=colnames) 
            data = data.append(data_new_line, ignore_index = True)
            return data
        
        data = data_()
        if drop_all:
            data.drop_duplicates(subset = "message", keep = 'last', inplace = True) 
        data.to_csv(os.path.join(dir_name, 'data_log_assistant.csv'), index=False)
                                       
        # split out and write data to time or location data files 
        if id_message == 'time':
            data = data_(id_message)
    
            if drop_time:
                data_drop = data.drop_duplicates(subset = "message", keep = 'last')
                data_drop.to_csv(os.path.join(dir_name, 'data_time_assistant.csv'), index = False)
            else:
                data.to_csv(os.path.join(dir_name, 'data_time_assistant.csv'), index = False)

        if id_message == 'location':    
            data = data_(id_message)

            if drop_loc:
                data_drop = data.drop_duplicates(subset = "message", keep = 'last')
                data_drop.to_csv(os.path.join(dir_name, 'data_location_assistant.csv'), index = False)
            else:
                data.to_csv(os.path.join(dir_name, 'data_location_assistant.csv'), index = False)

    def read_custom_command(self, dir_name):
        filename = os.path.join(dir_name, 'data_custom_command_assistant.csv')
        
        if not os.path.isfile(filename):
            colnames = ["date", "custom_command", "actual_command"]
            data = pd.DataFrame(columns=colnames)
            data.to_csv(filename, index = False)
        
        return pd.read_csv(filename)
                                       
    def search_data_time(self, data, start_time, end_time, date_type = 'date'):
        """search data by time"""
        self.date_except(date_type)
        selection = data[(data[date_type] >= start_time) & (data[date_type] <= end_time)]
        return selection

    def search_data_message(self, data, string):
        """search data by message""" 
        selection = data[data.message == string] 
        if self.isempty(selection):
            selection = self.search_data_string(data, string)
            selection = selection[selection['indexes'] != -1]
            selection.drop('indexes', axis=1, inplace=True)
        return selection
    
    def search_data_custom_message(self, data, string):
        """search data by message""" 
        selection = data[data.custom_command == string]
        selection = selection.actual_command.tolist() 
        return selection

    def search_data_string(self, data, string):
        """search data by string (part of the whole message)"""
        #data.dropna(inplace = True) 
        data['indexes'] = data['message'].str.find(string)  
        selection = data[data['indexes'] != -1]        
        data.drop('indexes', axis=1, inplace=True)
        return selection
    

    def input_message_auto_type(self):
        print('Input message')
        inputMessage = input()
        inputMessage_type = self.lang.get_type(inputMessage)
        print('The inputted message is:', inputMessage)
        while True:
            print('The inputted data type is:', inputMessage_type)
            if inputMessage_type == 'time':
                print('Input deadline')
                inputDeadline = input()
                print('The inputted deadline is:', inputDeadline)
                print('You choised:', inputMessage_type, 'message type is:', inputMessage_type)
                break
            if inputMessage_type == 'location':
                inputDeadline = np.nan
                print('You choised:', inputMessage_type, 'message type is:', inputMessage_type)
                break
        return inputMessage, inputMessage_type, inputDeadline

    def input_message(self):
        print('Input message')
        inputMessage = input()
        print('The inputted message is:', inputMessage)
        print('Input message type: 0 - means time, 1 - means location')
        while True:
            inputMessage_type = input()
            print('The inputted data type is:', inputMessage_type)
            if inputMessage_type == str(0):
                print('Input deadline')
                inputDeadline = input()
                print('The inputted deadline is:', inputDeadline)
                message_type = 'time'
                print('You choised:', inputMessage_type, 'message type is:', message_type)
                break
            if inputMessage_type == str(1):
                inputDeadline = np.nan
                message_type = 'location'
                print('You choised:', inputMessage_type, 'message type is:', message_type)
                break
            print('Input message type again: 0 - means time, 1 - means location')
        return inputMessage, message_type, inputDeadline

    def get_access(self,  dir_name, message, message_id, todo, search_string, start_time, end_time, deadline):
        
        if todo == MessageType.READ:

            if deadline != 'deadline':
                deadline = ''
            
            self.id_except(message_id)
            data = self.read_data(dir_name, message_id)
            
            if message_id == 'location' or message_id == 'log':
                # search_string start_time end_time Stencil (XXX)
                # 000
                if not search_string and not start_time and not end_time and not deadline:
                    return data
                # 100
                if not start_time and not end_time and not deadline:
                    return self.search_data_message(data, search_string)
                # 101
                if not start_time and end_time and not deadline:
                    data_time = self.search_data_time(data, str(datetime.datetime.min), end_time)
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 110
                if start_time and not end_time and not deadline:
                    data_time = self.search_data_time(data, start_time, str(datetime.datetime.max))
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 111
                if start_time and end_time and not deadline:
                    data_time = self.search_data_time(data, start_time, end_time)
                    data = self.search_data_message(data_time, search_string)
                    return data
            
            if message_id == 'time' or message_id == 'log':
                # search_string start_time end_time deadline. Stencil (XXXX)
                # 0000 0001
                if not search_string and not start_time and not end_time:
                    return data                
                # 0010
                if not search_string and not start_time and end_time and not deadline:
                    data = self.search_data_time(data, str(datetime.datetime.min), end_time)
                    return data
                # 0011
                if not search_string and not start_time and end_time and deadline:
                    data = self.search_data_time(data, str(datetime.datetime.min), end_time, 'deadline')
                    return data
                # 0100
                if not search_string and start_time and not end_time and not deadline:
                    data = self.search_data_time(data, start_time, str(datetime.datetime.max))
                    return data
                # 0101
                if not search_string and start_time and not end_time and deadline:
                    data = self.search_data_time(data, start_time, str(datetime.datetime.max), 'deadline')
                    return data
                # 0110
                if not search_string and start_time and end_time and not deadline:
                    data = self.search_data_time(data, start_time, end_time)
                    return data
                # 0111
                if not search_string and start_time and end_time and deadline:
                    data = self.search_data_time(data, start_time, end_time, 'deadline')
                    return data
                # 1000
                if search_string and not start_time and not end_time and not deadline:
                    data = self.search_data_message(data, search_string)
                    return data
                # 1001
                if search_string and not start_time and not end_time and deadline:
                    data = self.search_data_message(data, search_string)
                    return data
                # 1010
                if search_string and not start_time and end_time and not deadline:
                    data_time = self.search_data_time(data, str(datetime.datetime.min), end_time)
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 1011
                if search_string and not start_time and end_time and deadline:
                    data_time = self.search_data_time(data, str(datetime.datetime.min), end_time, 'deadline')
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 1100
                if search_string and start_time and not end_time and not deadline:
                    data_time = self.search_data_time(data, start_time, str(datetime.datetime.max))
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 1101
                if search_string and start_time and not end_time and deadline:
                    data_time = self.search_data_time(data, start_time, str(datetime.datetime.max), 'deadline')
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 1110
                if search_string and start_time and end_time and not deadline:
                    data_time = self.search_data_time(data, start_time, end_time)
                    data = self.search_data_message(data_time, search_string)
                    return data
                # 1111
                if search_string and start_time and end_time and deadline:
                    data_time = self.search_data_time(data, start_time, end_time, 'deadline')
                    data = self.search_data_message(data_time, search_string)
                    return data
                
        if todo == MessageType.WRITE:
            self.id_except(message_id)

            if message and not deadline:
                self.write_data(dir_name, message_id, message)
            if message and deadline:
                self.write_data(dir_name, message_id, message, deadline)

    def date_clean(self, dir_name, start_time, end_time, message_id):
        """clean data by the creation date"""
        self.id_except(message_id)
        data = self.read_data(dir_name, message_id)
        data.drop(data[(data['date'] >= start_time) & (data['date'] <= end_time)].index, inplace = True)
        data.to_csv('data/data_' + message_id + '_assistant.csv', index = False)

    def deadline_clean(self, dir_name, message_id, deadline = str(datetime.date.today())):
        """clean data by the deadline date"""
        self.id_except(message_id)
        data = self.read_data(dir_name, message_id)
        selection = data.dropna()
        if self.isempty(selection):
            pass
        else:
            data.drop(data[data['deadline'] < deadline].index, inplace = True) 
            data.to_csv('data/data_' + message_id + '_assistant.csv', index = False)

    def row_clean(self, dir_name, message_id, n = 1):
        self.id_except(message_id)
        data = self.read_data(dir_name, message_id)
        data.drop(data.tail(n).index, inplace = True)
        data.to_csv('data/data_' + message_id + '_assistant.csv', index = False)

    def find_period2message(self, message):
        result = ''
        # this list can be later imported from some data file
        time_list = ['yesterday', 'today', 'tomorrow', 'current week', 'last week', 'next week', 'last month',
                     'current month', 'next month', 'last year', 'current year', 'next year']
        
        for i in time_list:
            index = message.find(i)
            if index != -1:
                result = i
                break
                
        return str(result)

    def s2u(self, string):
        """Replace space with underscore"""
        # Leave only numbers and letters
        string = re.sub(r"[^\w\s]", '', string)
        # Replace space with underscore
        string = re.sub(r"\s+", '_', string)
        return string

    def message_processing(self, user_message):        
                
        if len(user_message.split()) == 1:
            todo = MessageType.READ
            message_id = 'log'
            subject = user_message
        else:
            todo = self.lang.get_type_question_answer(user_message) # read / write
            message_id = self.lang.get_type(user_message) # time / location
            subject = self.lang.get_subject(user_message)
        self.id_except(message_id) #check if message_id is correct
        try:
            time = self.lang.get_time(user_message)
        except:
            time = []
        dead_line = self.lang.date_deadline(user_message)
        return todo, message_id, subject, time, dead_line

    def save_user_input(self, dir_name, message):
        filename = 'user_input.txt'
        filepath = os.path.join(dir_name, filename)
        with open(filepath, 'a') as f:
            message +='\n'
            f.write(message)
    
    def all_recorded_messages(self, dir_name):
        """Returns all recorded messages from user's database.
        dir_name :: str, full path to user account folder"""
        message = subject = start_time = end_time = ''
        message_id = 'log'
        todo = MessageType.READ
        is_deadline = 'date'

        result = self.get_access(dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline)
        result.drop_duplicates(subset="message", keep='last', inplace=True)
        return result

    def latest_recorded_messages(self, dir_name, count):
        """Return latest x recorded messages, stored in the user's database.
        dir_name :: str, full path to user account folder
        count :: int, number of messages to be returned"""

        result = self.all_recorded_messages(dir_name)[-count:]
        return result

    def get_deadline(self, message, message_id, time):
        """Determine deadline in the given message"""
        start_time, end_time = self.find_time_interval(message, message_id, time)
        deadline = end_time or start_time.replace('23:59:59', '00:00:00')
        return deadline

    def find_time_interval(self, message, message_id, time):
        """Obtain time interval, start time and end time, from the message. 
        message_id shoud be equal to 'time'. Otherwise, there will be empty values."""
        start_time = end_time = ''
        if message_id == 'time':
            period = self.find_period2message(message)
            if period:
                start_time, end_time = eval('t.' + self.s2u(period) + '()')
            elif not period and len(time) == 2:
                start_time = time[0]
                end_time = time[1]
        return start_time, end_time
