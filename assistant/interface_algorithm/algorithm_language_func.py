#!/usr/bin/env python
# coding: utf-8

import pickle
import datetime
import re
import dateparser

import spacy
from spacy.tokens import Span
from spacy.language import Language

from assistant.interface_algorithm.helper_func import Helpers
from assistant.message_type import MessageType

nlp = spacy.load("en_core_web_trf")

# for get_time function
@Language.component("date_ner")
def date_ner(doc):
    pattern = "\d+[a-zA-Z]+.?\s\w+" #11th November
    #pattern = "\b\d{1,2}([a-zA-Z]+\s|\s)\w+"
    original_ents = list(doc.ents)
    mwt_ents = []
    for match in re.finditer(pattern, doc.text):
        start, end = match.span()
        span = doc.char_span(start, end)
        if span is not None:
            mwt_ents.append((span.start, span.end, span.text))
    for ent in mwt_ents:
        start, end, name = ent
        per_ent = Span(doc, start, end, label="DATE")
        original_ents.append(per_ent)
    doc.ents = original_ents
    return doc


# Algorithm_language_func class declaration
class Algorithm_language_func():
    def __init__(self,
                 helpers : Helpers,
                 saved_model=None,
                 saved_model2=None,
                 saved_model3=None):
         
        self.saved_model = saved_model if saved_model else helpers.model_path('model_get_type_21.12.pkl')

        self.saved_model2 = saved_model2 if saved_model2 else helpers.model_path('model_getQA_14.12.pkl')

        self.saved_model3 = saved_model3 if saved_model3 else helpers.model_path('model3.pkl')


    def date_deadline(self, message):
        with open(self.saved_model3, 'rb') as file:
            model = pickle.load(file)
        if list(model.predict([message])) == [0]:
            #return "date"
            return ""
        if list(model.predict([message])) == [1]:
            return "deadline"
        return None
    
    def get_type(self, message):
        with open(self.saved_model, 'rb') as file:
            model = pickle.load(file)
        if list(model.predict([message])) == [0]:
            return "location"
        if list(model.predict([message])) == [1]:
            return "time"
        return None

    def get_type_question_answer(self, message):
        with open(self.saved_model2, 'rb') as file:
            model = pickle.load(file)
        if list(model.predict([message])) == [1]:
            return MessageType.READ
        if list(model.predict([message])) == [0]:
            return MessageType.WRITE
        return None

    def get_subject(self, message): #for location questions
        message = " ".join(message.split()) #normalize whitespace chars
        doc = nlp(message)
        deps_for_modifiers = ['amod','compound', 'poss', 'det'] #e.g. my art box with yellow paintings
        deps = ['nsubj','dobj','conj']
        if 'dobj' in [chunk.root.dep_ for chunk in doc.noun_chunks]: #e.g. where did I + Verb
            if len(list(chunk for chunk in doc.noun_chunks if chunk.root.dep_== 'nsubj')) == 1:
                if 'prep' and 'pobj' in [chunk.root.dep_ for chunk in doc.noun_chunks]: #cup for coffe, box with smth
                    deps_mod = ['dobj','conj', 'prep', 'pobj'] + deps_for_modifiers
                    deps_subj = ['nsubj']          
                    noun_prep = [token.text for token in doc if token.dep_ in deps_mod]
                    subj = [token.text for token in doc if token.dep_ in deps_subj]
                    result = [' '.join(subj)] + [' '.join(noun_prep)]
                    return(result)
                else:
                    result = [chunk.text for chunk in doc.noun_chunks if chunk.root.dep_ in deps]
            else:
                result = [] #note: keep in mind to..
        else: #e.g. where is...
            deps = ['attr','nsubj','conj','nsubjpass'] + deps_for_modifiers        
            if len(list(chunk for chunk in doc.noun_chunks if chunk.root.dep_== 'nsubj')) == 1:
                if 'prep' and 'pobj' in [chunk.root.dep_ for chunk in doc.noun_chunks]: #cup for coffe, box with smth
                    deps = ['nsubj','prep','pobj'] + deps_for_modifiers ##? check
                    noun_prep = [token.text for token in doc if token.dep_ in deps]
                    result = [' '.join(noun_prep)]
                    return(result)
            result = [chunk.text for chunk in doc.noun_chunks if chunk.root.dep_ in deps]
        return(result)

    def get_time(self, message):
        if 'entity_ruler' in nlp.pipe_names:
            nlp.remove_pipe("entity_ruler")
        message = " ".join(message.split()) #normalize whitespaces
        result = ['', '']
        ruler = nlp.add_pipe("entity_ruler",before='ner')
        patterns = [{"label": "YEAR", "pattern": [{"TEXT" : "this"},{"TEXT" : "year"}]}] # match year e.g. this year                                                     
        ruler.add_patterns(patterns)
        doc = nlp(message)
        entities = [ent.text for ent in doc.ents if ent.label_ == 'DATE' or ent.label_ == 'TIME']
        if entities:
            parsed_date = dateparser.parse(" ".join(entities))
            if parsed_date: # date was recognized by dateparser
                for ent in doc.ents:
                    if ent.label_ == 'DATE' or ent.label_ == 'TIME' and ent.start != 0:
                        prev_token = doc[ent.start - 1]
                        if prev_token.text == "before" or prev_token.text == "until":
                            result = ['', parsed_date.strftime('%Y-%m-%d %H:%M:%S')]
                        elif prev_token.text == "after":
                            result = [parsed_date.strftime('%Y-%m-%d %H:%M:%S'), '']
                        else:
                            result = [parsed_date.strftime('%Y-%m-%d %H:%M:%S'), '']
                        break
        if result == ['', '']: # between dates, not recognized by spacy
            result = []
            if entities:
                if 'entity_ruler' in nlp.pipe_names: #reload pipeline
                    nlp.remove_pipe("entity_ruler")
                message = message.replace('between','')
                message = " ".join(message.split()) #normalize whitespaces
                ruler = nlp.add_pipe("entity_ruler",before='ner')
                patterns = [{"label": "YEAR", "pattern": [{"TEXT" : {"REGEX": "^\d\d\d\d$"}}]},
                        {"label": "NL_YEAR", "pattern": [{"TEXT" : "this"},{"TEXT" : "year"}]}]# match year #NL_year not used so far                                       
                ruler.add_patterns(patterns)
                doc = nlp(message)
                years = [ent for ent in doc.ents if ent.label_ == 'YEAR' or ent.label_ == 'NL_YEAR']
                dates = [ent for ent in doc.ents if ent.label_ == 'DATE' or ent.label_ == 'TIME' and ent.start != 0]
                num_years = len(years)
                num_dates = len(dates)        
                parsed_dates = []
                if num_years == 0: #11th November and 13th November
                    for date in dates:
                        parsed_date = dateparser.parse(date.text)
                        parsed_dates.append(parsed_date)                
                    result.append(parsed_dates[0].strftime('%Y-%m-%d %H:%M:%S'))
                    result.append(((parsed_dates[1]  - datetime.timedelta(seconds=1) + datetime.timedelta(days=1))).strftime('%Y-%m-%d %H:%M:%S'))
                    return result
                if num_years == 2 and num_dates == 1: #11th November and 13th November 2021-2020
                    nlp_dates = spacy.blank("en")
                    nlp_dates.add_pipe("date_ner")
                    doc_dates = nlp_dates(message)
                    dates = [ent for ent in doc_dates.ents if ent.label_ == 'DATE' or ent.label_ == 'TIME' and ent.start != 0]
                    for year, date in zip(years, dates):
                        full_date = " ".join([date.text, year.text])
                        parsed_date = dateparser.parse(full_date)
                        parsed_dates.append(parsed_date)
                    result.append(parsed_dates[0].strftime('%Y-%m-%d %H:%M:%S'))
                    result.append(((parsed_dates[1]  - datetime.timedelta(seconds=1) + datetime.timedelta(days=1))).strftime('%Y-%m-%d %H:%M:%S'))
                    return result
                if num_years == 1 and num_dates == 2: #11th November 2021 and 13th November 
                    for date in dates:
                        date = " ".join([date.text, years[0].text])
                        parsed_date = dateparser.parse(date)
                        parsed_dates.append(parsed_date)       
                    result.append(parsed_dates[0].strftime('%Y-%m-%d %H:%M:%S'))
                    result.append(((parsed_dates[1]  - datetime.timedelta(seconds=1) + datetime.timedelta(days=1))).strftime('%Y-%m-%d %H:%M:%S'))
                    return result  
                if num_years == 1 and num_dates == 1: #11th November and 13th November 2021
                    nlp_dates = spacy.blank("en")
                    nlp_dates.add_pipe("date_ner")
                    doc_dates = nlp_dates(message)
                    dates = [ent for ent in doc_dates.ents if ent.label_ == 'DATE' or ent.label_ == 'TIME' and ent.start != 0]
                    for date in dates:
                        date = " ".join([date.text, years[0].text])
                        parsed_date = dateparser.parse(date)
                        parsed_dates.append(parsed_date)
                    result.append(parsed_dates[0].strftime('%Y-%m-%d %H:%M:%S'))
                    result.append(((parsed_dates[1]  - datetime.timedelta(seconds=1) + datetime.timedelta(days=1))).strftime('%Y-%m-%d %H:%M:%S'))
                    return result
                if num_years == 2: # 11th November 2021 and 13th November 2021 
                    for year, date in zip(years, dates):
                        full_date = " ".join([date.text, year.text])
                        parsed_date = dateparser.parse(full_date)
                        parsed_dates.append(parsed_date)
                    result.append(parsed_dates[0].strftime('%Y-%m-%d %H:%M:%S'))
                    result.append(((parsed_dates[1]  - datetime.timedelta(seconds=1) + datetime.timedelta(days=1))).strftime('%Y-%m-%d %H:%M:%S'))
                else:
                    result = []
        return result