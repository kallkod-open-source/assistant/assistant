#!/usr/bin/env python
# coding: utf-8

import unittest

from assistant.interface_algorithm import algorithm_language_func
from assistant.message_type import MessageType
from assistant.interface_algorithm.helper_func import Helpers

helpers = Helpers()
lang = algorithm_language_func.Algorithm_language_func(helpers)


class Test_Language_func_Subject(unittest.TestCase):
    def test_get_subject(self):
        result = lang.get_subject('where are ball, car and toy?')
        self.assertEqual(type(result), list)
        self.assertEqual(len(result), 3) 
        self.assertEqual(result[0], 'ball')
        self.assertEqual(result[1], 'car')
        self.assertEqual(result[2], 'toy')

        result = lang.get_subject('where is ball')
        self.assertEqual(type(result), list)
        self.assertEqual(len(result), 1)
        self.assertEqual(result[0], 'ball')
        
        subject = lang.get_subject('where is ball, car and toy?')
        self.assertEqual(type(subject), list)
        self.assertEqual(len(subject), 3)
        self.assertEqual(subject[0], 'ball')
        self.assertEqual(subject[1], 'car')
        self.assertEqual(subject[2], 'toy')
        
        messages_list = ['', ' ', 'where are', 'where is']
        
        for message in messages_list:
            self.assertEqual(type(message), str)
            subject = lang.get_subject(message)
            self.assertEqual(type(subject), list)
            self.assertEqual(len(subject), 0)


class Test_Language_func(unittest.TestCase):
    def test_message(self):
        message = 'where is ball, car and toy?'

        todo = lang.get_type_question_answer(message)
        self.assertEqual(todo, MessageType.READ)
        message_id = lang.get_type(message)
        self.assertEqual(message_id, 'location')
        
        
        message = 'what do I need to do tonight?'
        
        todo = lang.get_type_question_answer(message)
        self.assertEqual(todo, MessageType.READ)
        message_id = lang.get_type(message)
        self.assertEqual(message_id, 'time')
        

        message = 'I need to cook dinner'
        
        todo = lang.get_type_question_answer(message)
        self.assertEqual(todo, MessageType.WRITE)
        message_id = lang.get_type(message)
        self.assertEqual(message_id, 'time')


class Test_Language_func_Time(unittest.TestCase):
    def test_get_time(self):
        message = 'Find events between 11th November and 13th November'
        time = lang.get_time(message)
        self.assertEqual(time, ['2022-11-11 00:00:00', '2022-11-13 23:59:59'])

        message = "Find events after 11th November 2021"
        time = lang.get_time(message)
        self.assertEqual(time, ['2021-11-11 00:00:00', ''])

        message = "the annual concert will take place on October 30th this year"
        time = lang.get_time(message)
        self.assertEqual(time, ['2022-10-30 00:00:00', ''])


class Test_Expected_Failure(unittest.TestCase):
    @unittest.expectedFailure
    def test_get_time_which_fails(self):
        message = "Find events before 11th November 2021"
        time = lang.get_time(message)
        self.assertEqual(time, ['', '2021-11-11 00:00:00'])


if __name__ == '__main__':
    unittest.main()
