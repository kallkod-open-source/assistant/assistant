#!/usr/bin/env python
# coding: utf-8
import unittest
import os
import pandas as pd
import numpy as np
import datetime
import math

from assistant.interface_algorithm import algorithm_func
from assistant.interface_algorithm import algorithm_time_func
from assistant.interface_algorithm import algorithm_language_func
from assistant.interface_algorithm.helper_func import Helpers

helpers = Helpers()
algt = algorithm_time_func.Algorithm_time_func()
algl = algorithm_language_func.Algorithm_language_func(helpers)
alg = algorithm_func.Algorithm_func(helpers, algl)

error_message = 'Test value is not true.'

class Test_IO_Algorithm_Func(unittest.TestCase):
    def test_read_and_write_data(self):
        
        dir_path = 'data'        
        if os.path.isdir(dir_path):
            alg.rm_dirr(dir_path)
            
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-15')
        alg.write_data(dir_path, 'time', 'repair bicycle', '2020-05-17')
        alg.write_data(dir_path, 'location', 'car in the garrage')
        
        result = alg.read_data(dir_path, 'log')
        self.assertEqual(result.shape[0], 3)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2020-01-15')
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[1]['deadline'], '2020-05-17')
        self.assertEqual(result.iloc[2]['message_id'], 'location')
        self.assertEqual(result.iloc[2]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[2]['deadline']), error_message)
        
        result = alg.read_data(dir_path, 'time')
        self.assertEqual(result.shape[0], 2)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2020-01-15')
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[1]['deadline'], '2020-05-17')
        
        result = alg.read_data(dir_path, 'location')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)
        
        # remove data files after reading
        alg.rm_dirr(dir_path)

    def test_write_default_data(self):
        
        dir_path = 'data'
        if os.path.isdir(dir_path):
            alg.rm_dirr(dir_path)
        
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-15')
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-16', drop_time = False)
        alg.write_data(dir_path, 'location', 'car in the garrage', drop_time = False)
        alg.write_data(dir_path, 'location', 'car in the garrage', drop_loc = False)
    
        result = alg.read_data(dir_path, 'log')
        self.assertEqual(result.shape[0], 4)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2020-01-15')
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'make soup')
        self.assertEqual(result.iloc[1]['deadline'], '2020-01-16')
        self.assertEqual(result.iloc[2]['message_id'], 'location')
        self.assertEqual(result.iloc[2]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[2]['deadline']), error_message)
        self.assertEqual(result.iloc[3]['message_id'], 'location')
        self.assertEqual(result.iloc[3]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[3]['deadline']), error_message)
        
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-17', drop_all = True)
        
        result = alg.read_data(dir_path, 'log')
        self.assertEqual(result.shape[0], 2)
        self.assertEqual(result.shape[1], 4)

        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'make soup')
        self.assertEqual(result.iloc[1]['deadline'], '2020-01-17')
        
        # remove data files after reading
        alg.rm_dirr(dir_path)

class Test_IO_Algorithm_clean_Func(unittest.TestCase):
            
    def test_deadline_clean(self):
        
        dir_path = 'data'
        if os.path.isdir(dir_path):
            alg.rm_dirr(dir_path)
            
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-15')
        alg.write_data(dir_path, 'time', 'repair bicycle', '2020-05-17')
        alg.write_data(dir_path, 'location', 'car in the garrage')
        
        deadline = '2020-05-01'
        alg.deadline_clean(dir_path, 'time', deadline)
        
        result = alg.read_data(dir_path, 'time')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[0]['deadline'], '2020-05-17')
        
        # remove data files after reading
        alg.rm_dirr(dir_path)

class Test_IO_Algorithm_clean_Func(unittest.TestCase):
            
    def test_row_clean(self):
        
        dir_path = 'data'
        if os.path.isdir(dir_path):
            alg.rm_dirr(dir_path)
            
        alg.write_data(dir_path, 'time', 'make soup', '2020-01-15')
        alg.write_data(dir_path, 'time', 'repair bicycle', '2020-05-17')
        alg.write_data(dir_path, 'location', 'car in the garrage')
        
        alg.row_clean(dir_path, 'time')
        
        result = alg.read_data(dir_path, 'time')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2020-01-15')
        
        alg.row_clean(dir_path, 'time', 1)
        
        result = alg.read_data(dir_path, 'time')
        self.assertEqual(result.shape[0], 0)
        self.assertEqual(result.shape[1], 4)
        
        alg.row_clean(dir_path, 'location', 3)
        
        result = alg.read_data(dir_path, 'location')
        self.assertEqual(result.shape[0], 0)
        self.assertEqual(result.shape[1], 4)
                
        # remove data files after reading
        alg.rm_dirr(dir_path)

data = pd.read_csv('mock_data/mock_data.csv')
data = data[data.message_id == 'time']

class Test_Search_Algorithm_Func(unittest.TestCase):
    def test_search_data_time_0(self):
        result = alg.search_data_time(data, '2020-01-31', '2020-02-02')
        self.assertEqual(result.shape[0], 0)
        self.assertEqual(result.shape[1], 4)
                
    def test_search_data_time(self):
        result = alg.search_data_time(data, '2021-01-31', '2021-02-02')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')

    def test_search_data_time_default(self):
        result = alg.search_data_time(data, '2021-01-31', '2021-02-02', 'date')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')
        
    def test_search_data_time_date(self):
        result = alg.search_data_time(data, '2021-01-31', '2021-02-06')
        self.assertEqual(result.shape[0], 2)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[1]['deadline'], '2021-02-12')
        
    def test_search_data_time_deadline(self):
        result = alg.search_data_time(data, '2021-02-02', '2021-02-13', 'deadline')
        self.assertEqual(result.shape[0], 2)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')
        self.assertEqual(result.iloc[1]['message_id'], 'time')
        self.assertEqual(result.iloc[1]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[1]['deadline'], '2021-02-12')
        
    def test_search_data_time_deadline_current_week(self):
        today = algt.str2daytime('2021-02-02')
        dates = algt.current_week(today)
        result = alg.search_data_time(data, dates[0], dates[1], 'deadline')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')
        
    def test_search_data_time_deadline_next_week(self):
        today = datetime.datetime(2021,1,28,14,30)
        dates = algt.next_week(today)
        result = alg.search_data_time(data, dates[0], dates[1], 'deadline')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')

data = pd.read_csv('mock_data/mock_data.csv')

class Test_Search_Message_Algorithm_Func(unittest.TestCase):        
    def test_search_data_string_0(self):
        result = alg.search_data_string(data, 'hello')
        self.assertEqual(result.shape[0], 0)
        self.assertEqual(result.shape[1], 5)
        
    def test_search_data_string_1(self):
        result = alg.search_data_string(data, 'car')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 5)
        
        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)
        
    def test_search_data_string_2(self):
        result = alg.search_data_string(data, 'cycle')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 5)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'repair bicycle')
        self.assertEqual(result.iloc[0]['indexes'], 9)
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-12')
                
    def test_search_data_full_message_1(self):
        result = alg.search_data_message(data, 'car in the garrage')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)

    def test_search_data_full_message_2(self):
        result = alg.search_data_message(data, 'make soup')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')
        
    def test_search_data_part_message_1(self):
        result = alg.search_data_message(data, 'garrage')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)

    def test_search_data_part_message_2(self):
        result = alg.search_data_message(data, 'car')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'location')
        self.assertEqual(result.iloc[0]['message'], 'car in the garrage')
        self.assertTrue(math.isnan(result.iloc[0]['deadline']), error_message)
        
    def test_search_data_part_message_3(self):
        result = alg.search_data_message(data, 'soup')
        self.assertEqual(result.shape[0], 1)
        self.assertEqual(result.shape[1], 4)
        
        self.assertEqual(result.iloc[0]['message_id'], 'time')
        self.assertEqual(result.iloc[0]['message'], 'make soup')
        self.assertEqual(result.iloc[0]['deadline'], '2021-02-03')

class Test_Character_Replace_Func(unittest.TestCase):
    def test_s2u(self):    
        string_old_01 = 'Hello This is Test'
        string_old_02 = 'Hello This is Test 123'
        
        string_new_01 = alg.s2u(string_old_01)
        self.assertEqual(string_new_01, 'Hello_This_is_Test')
        
        string_new_02 = alg.s2u(string_old_02)
        self.assertEqual(string_new_02, 'Hello_This_is_Test_123')

class Test_Language_Func(unittest.TestCase):
    def test_get_type(self):
        result = algl.get_type('I need to find ball')
        self.assertEqual(result, 'time')
        
        result = algl.get_type('find ball')
        self.assertEqual(result, 'time')
        
        result = algl.get_type('car is in the garage')
        self.assertEqual(result, 'location')
        
        result = algl.get_type('nails is in the black box')
        self.assertEqual(result, 'location')

today = datetime.datetime(2011,9,1,0,0)

class Test_Algorithm_Time_Func(unittest.TestCase):
    def test_today_sharp(self):
        result = algt.today(today)
        self.assertEqual(result[0], '2011-09-01 00:00:00')
        self.assertEqual(result[1], '2011-09-01 23:59:59')
        
    def test_tomorrow(self):
        result = algt.tomorrow(today)
        self.assertEqual(result[0], '2011-09-02 00:00:00')
        self.assertEqual(result[1], '2011-09-02 23:59:59')
        
    def test_yesterday(self):
        result = algt.yesterday(today)
        self.assertEqual(result[0], '2011-08-31 00:00:00')
        self.assertEqual(result[1], '2011-08-31 23:59:59')
        
    def test_current_week(self):
        result = algt.current_week(today)
        self.assertEqual(result[0], '2011-09-01 00:00:00')
        self.assertEqual(result[1], '2011-09-04 23:59:59')
        
    def test_next_week(self):
        result = algt.next_week(today)
        self.assertEqual(result[0], '2011-09-05 00:00:00')
        self.assertEqual(result[1], '2011-09-11 23:59:59')
        
    def test_last_week(self):
        result = algt.last_week(today)
        self.assertEqual(result[0], '2011-08-22 00:00:00')
        self.assertEqual(result[1], '2011-08-28 23:59:59')
        
    def test_current_month(self):
        result = algt.current_month(today)
        self.assertEqual(result[0], '2011-09-01 00:00:00')
        self.assertEqual(result[1], '2011-09-30 23:59:59')
    
    def test_next_month(self):
        result = algt.next_month(today)
        self.assertEqual(result[0], '2011-10-01 00:00:00')
        self.assertEqual(result[1], '2011-10-31 23:59:59')
        
    def test_last_month(self):
        result = algt.last_month(today)
        self.assertEqual(result[0], '2011-08-01 00:00:00')
        self.assertEqual(result[1], '2011-08-31 23:59:59')
        
    def test_current_year(self):
        result = algt.current_year(today)
        self.assertEqual(result[0], '2011-09-01 00:00:00')
        self.assertEqual(result[1], '2011-12-31 23:59:59')
        
    def test_last_year(self):
        result = algt.last_year(today)
        self.assertEqual(result[0], '2010-01-01 00:00:00')
        self.assertEqual(result[1], '2010-12-31 23:59:59')
        
    def test_next_year(self):
        result = algt.next_year(today)
        self.assertEqual(result[0], '2012-01-01 00:00:00')
        self.assertEqual(result[1], '2012-12-31 23:59:59')

if __name__ == '__main__':
    unittest.main()

