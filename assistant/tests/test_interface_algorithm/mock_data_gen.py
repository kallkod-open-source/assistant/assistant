#!/usr/bin/env python
# coding: utf-8

# In[1]:


# Generate mock data for unit test
import pandas as pd
import numpy as np
    
time_01 = '2021-02-01'
time_02 = '2021-02-05'
time_03 = '2021-02-15'

message_01 = 'make soup'
message_02 = 'repair bicycle'
message_03 = 'car in the garrage'
message_id = 'time'
message_id_loc = 'location'
message_id_time = 'time'
deadline_01 = '2021-02-03'
deadline_02 = '2021-02-12'
deadline_03 = np.nan


colnames = ["date","message_id", "message", "deadline"]
data = pd.DataFrame(columns=colnames)
data1 = pd.DataFrame([[time_01, message_id, message_01, deadline_01]], columns=colnames)
data = data.append(data1, ignore_index = True)
data2 = pd.DataFrame([[time_02, message_id_time, message_02, deadline_02]], columns=colnames)
data = data.append(data2, ignore_index = True)
data3 = pd.DataFrame([[time_03, message_id_loc, message_03, deadline_03]], columns=colnames)
data = data.append(data3, ignore_index = True)

data.to_csv('mock_data/mock_data.csv', index = False)

