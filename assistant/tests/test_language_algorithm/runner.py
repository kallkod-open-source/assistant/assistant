#!/usr/bin/env python
# coding: utf-8

from datetime import datetime as dt
d1 = dt.now()

from assistant.tests.test_language_algorithm import test


# names of the tests
test_get_type = test.test_get_type
test_get_type_qa = test.test_get_type_qa
test_get_subject = test.test_get_subject
test_get_time_simpledates = test.test_get_time_simpledates
test_date_deadline = test.test_date_deadline

# utility program names
loader = test.loader
FileWriter = test.FileWriter
print_summary = test.print_summary


# create instanse of FileWriter
f = FileWriter()

# DBs
# x = loader('mock_database/all_database_no_notes.csv')
# t1 = loader('mock_database/get_type_130921.csv')   # contains note, location and time
# gt_location = loader('mock_database/new_data/location_new.csv')
# gt_note =  loader('mock_database/new_data/notes_new.csv')
# gt_time =  loader('mock_database/new_data/time_new.csv')
t1 = loader('mock_database/get_time_simpledb.csv')
t2 = loader('mock_database/simple_dates.csv')
d = loader('mock_database/date_deadline.csv')

def run(test_name, csv_database, filename=""):
    """Run test <test_name> using <csv_database>. 
    Use <filename> for the output file, if it isn't empty string"""
    values = test_name(csv_database) # run test
    print_summary(*values)           # print info on the screen
    f.write(*values[:-1], filename)  # write output files

def run_tests():
    run(test_get_time_simpledates, t1)
    run(test_get_time_simpledates, t2)

    run(test_date_deadline, d)

    ## tests for functions which use spacy
    # test(test_get_subject, x)


run_tests()


d2 = dt.now()
print('Tests execution time:', d2 - d1)

