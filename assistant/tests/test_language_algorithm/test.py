#!/usr/bin/env python
# coding: utf-8

from datetime import datetime as dt
from pathlib import Path, PurePath
import hashlib
import csv
from assistant.interface_algorithm import algorithm_language_func
from assistant.interface_algorithm.helper_func import Helpers

helpers = Helpers()
lang = algorithm_language_func.Algorithm_language_func(helpers)
nlp = algorithm_language_func.nlp
lang_test_path = PurePath(helpers.package_path(), 'tests/test_language_algorithm/')

'''# available language functions that could be tested:
get_subject, get_time, get_type, get_type_question_answer, date_deadline
# models:
model3.pkl  -  date_deadline()
model_get_type_21.12.pkl  -  get_type()
model_getQA_14.12.pkl  -  get_type_question_answer()

# md5 hash
model3.pkl cf8dcf9ac060cac93441b7248581c7fe
model_get_type_21.12.pkl 48b2cc8bbc6206773ff5403d33e12c3a
model_getQA_14.12.pkl a3e0802ce33a296f2544c7ea021b65ac'''


def fname(test_name: str):
    """ Create filename with the name of tested function in format, 
    like "_get_date_10112021_134755_404838.csv". Description:
    _[name_of_the_function]_[date ddmmyyyy]_[time HHMMSS]_[time in microseconds].csv """
    dt_string = dt.now().strftime("_%d%m%Y_%H%M%S_%f")
    res = '_' + test_name + dt_string + '.csv'
    return res


def loader(csv_database: str):
    """ Open csv database and return list, created from this database.
    csv_database :: path, consisting only from the folder name and the 
        file name, like 'mock_database/get_time_simpledb.csv'. Path to
        the main test directory is added automatically.
    """
    full_csv_database_path = PurePath(lang_test_path, csv_database)
    with open(full_csv_database_path, newline='') as f:
        reader = csv.reader(f)
        next(reader)
        return list(reader)


def hash_info(model: str):
    """ Open model file and check MD5 hashsum """
    md5_hash = hashlib.md5()
    with open(model, 'rb') as file:
        content = file.read()
        md5_hash.update(content)
        digest = md5_hash.hexdigest()  # type str
        return digest


def test_get_type(iterable: list):
    """ docstring """
    output_info = []  # for output files
    errors_counter = empty_str_counter = 0

    for row in iterable:
        message = row[0]
        expected_result = row[1]
        resp = None
        resp = lang.get_type(message)
        mylist = [message, expected_result, resp]

        if expected_result != resp:
            mylist.append('error')
            errors_counter += 1

        if resp is None:
            empty_str_counter += 1

        output_info.append(mylist)
    return output_info, len(iterable), errors_counter, 'get_type', empty_str_counter


def test_get_type_qa(iterable: list):
    """ docstring """
    output_info = []  # for output files
    errors_counter = empty_str_counter = 0

    for row in iterable:
        message = row[0]
        expected_result = row[2]
        resp = None
        resp = lang.get_type_question_answer(message)
        mylist = [message, expected_result, resp]

        if expected_result != resp:
            mylist.append('error')
            errors_counter += 1

        if resp is None:
            empty_str_counter += 1

        output_info.append(mylist)
    return output_info, len(iterable), errors_counter, 'get_type_question_answer', empty_str_counter


def test_get_subject(iterable: list):
    """ docstring

    # output_info structure is:
    # [[<elem1>], [<elem2>], ... [<elemN>]]
    <elem> structure is:
    ['message', 'expected result', 'received result', 'error']
    # 'error' string is oprional """
    output_info = []  # for output files
    empty_str_counter = errors_counter = 0

    for row in iterable:
        message = row[0]
        expected_result = row[3]
        resp = ''
        resp = lang.get_subject(message)
        mylist = [message, expected_result, resp]

        if len(resp) == 0:  # count empty subjects
            empty_str_counter += 1

        if isinstance(resp, str) and expected_result != resp:
            mylist.append('error')
            errors_counter += 1

        if isinstance(resp, list):
            list_to_str = '; '.join(map(str, resp))
            mylist[2] = list_to_str
            if expected_result != list_to_str:
                mylist.append('error')
                errors_counter += 1

        output_info.append(mylist)
    return output_info, len(iterable), errors_counter, 'get_subject', empty_str_counter


def test_get_time_simpledates(iterable: list):
    """ This test checks messages with simple dates,
    which include day, month, year and time, if time is specified."""
    output_info = []  # for output files
    empty_str_counter = errors_counter = 0

    for row in iterable:
        message = row[0]
        expected_result = row[4]
        resp = ["You shouldn't see this message"]
        
        try:
            resp = lang.get_time(message)
        except Exception as oops:
            resp = "Error: exception occurred"
            print(f'Exception occured! Message: "{message}"  String №{iterable.index(row) + 2}')
            print(oops)
            print()

        mylist = [message, expected_result, resp]

        if expected_result != str(resp):
            mylist.append('error')
            errors_counter += 1

        if resp in (['', ''], []):  # count empty results
            empty_str_counter += 1

        output_info.append(mylist)
    return output_info, len(iterable), errors_counter, 'get_time', empty_str_counter


def test_date_deadline(iterable: list):
    ''' docstring '''
    output_info = []   # for output files
    errors_counter = empty_str_counter = 0

    for row in iterable:
        message = row[0]
        expected_result = row[6]
        resp = ""
        resp = lang.date_deadline(message)  # type str
        mylist = [message, expected_result, resp]

        if expected_result != resp:
            mylist.append('error')
            errors_counter += 1

        if not resp:
            empty_str_counter += 1

        output_info.append(mylist)
    return output_info, len(iterable), errors_counter, 'date_deadline', empty_str_counter

class FileWriter:
    def __init__(self):
        self.nlp_name = Path(nlp.path).name  # en_core_web_trf-3.0.0 on 22.06.2021
        self.method = {
            'get_subject': FileWriter.on_get_subject,
            'get_type': FileWriter.on_get_type,
            'get_type_question_answer': FileWriter.on_get_type_question_answer,
            'get_time': FileWriter.on_get_time,
            'date_deadline':FileWriter.on_date_deadline,
        }
        # these variables are used to point to the model file.
        # get_type(), get_type_question_answer(), date_deadline() use models.
        # Also, the output files take some information from these models.
        self.model_get_type = lang.saved_model
        self.model_get_type_question_answer = lang.saved_model2
        self.model_date_deadline = lang.saved_model3

    def prepare(self, test_name):
        """find method by the name, and call it"""
        x = self.method[test_name]
        x(self)

    def on_get_type(self):
        model = Path(self.model_get_type)
        checksum = hash_info(model)
        self.used_module = f'{model.name} \t md5 checksum: {checksum}'

    def on_get_type_question_answer(self):
        model = Path(self.model_get_type_question_answer)
        checksum = hash_info(model)
        self.used_module = f'{model.name} \t md5 checksum: {checksum}'

    def on_get_subject(self):
        self.used_module = f'nlp version: {self.nlp_name}'

    def on_get_time(self):
        self.used_module = f'nlp version: {self.nlp_name}'

    def on_date_deadline(self):
        model = Path(self.model_date_deadline)
        checksum = hash_info(model)
        self.used_module = f'{model.name} \t md5 checksum: {checksum}'

    def write(self, output_info, msg_counter, errors_counter, test_name, filename=''):  # msg_counter == len(iterable)
        """This method is used to write data, received from tests, to csv files. Two files are created:
        with all received data "full_<...>.csv", and with errors data only: "errors_<...>.csv".
        output_info :: list with output data to write.
        msg_counter, errors_counter :: message counter and error counter, respectively
        test_name :: the name of the function that was tested. Used to prepare summary information for output files
        filename :: different name for output file. Note, that the filename will be preceded by a prefix 'full' or 'errors',
        and you have to specify '.csv' extension in filename, for instance: errors_your_filename.csv. The output will create
        two files, with all data and only with errors data.
        """
        self.prepare(test_name)

        # variables for filenames
        if filename:
            file_name = filename
        else:
            file_name = fname(test_name)
        output_dir = 'output'
        output_dir_path = PurePath(lang_test_path, output_dir)
        name_err = 'errors' + file_name
        name = 'full' + file_name

        # check if the folder exists, create it if doesn't
        Path(output_dir_path).mkdir(exist_ok=True)

        new_full_file = PurePath(output_dir_path).joinpath(name)
        new_error_file = PurePath(output_dir_path).joinpath(name_err)

        with open(new_full_file, 'w', newline='') as fw, \
                open(new_error_file, 'w', newline='') as fw_e:

            writer = csv.writer(fw, quoting=csv.QUOTE_ALL)  # full<...>csv
            errors_writer = csv.writer(fw_e, quoting=csv.QUOTE_ALL)  # errors<...>csv
            # variables with the necessary information for the output files
            headers = ['MESSAGE', 'EXPECTED RESULT', 'RECEIVED RESULT', 'WARNINGS']

            summary = f'Tested function {test_name}. \n' \
                      f'Used:\t{self.used_module}\n' \
                      f'Messages total: {msg_counter}, \n' \
                      f'Errors: {str((errors_counter / msg_counter) * 100)[:5]}%, {errors_counter} messages'
            # write information to files
            writer.writerow([summary, '', ''])
            writer.writerow(headers)

            errors_writer.writerow([summary + '\n!!! This file contains only errors!', '', ''])
            errors_writer.writerow(headers[:3])

            for item in output_info:
                writer.writerow(item)  # write to "full" file
                if len(item) == 4:
                    errors_writer.writerow(item[:3])  # write to "errors" file
            # display info about created files
            print(f'File "{name}" was successfully generated \n'
                  f'File "{name_err}" was successfully generated \n')


def print_summary(output_info, msg_counter, errors_counter, test_name, empty_str_counter):
    """ docstring """
    print('* ' * 10, f'Tested function {test_name}')
    if errors_counter > 0:
        print('Errors detected!')
        print('_' * 30)
        # displaying errors on the screen, line by line
        if errors_counter > 100:
            print('! Too much strings to print (more than 100).\nAll test output data was saved to files.')
        elif errors_counter <= 100:
            for i in output_info:
                if len(i) == 4:
                    msg1 = "'{}' '{}' '{}'  - on string {}".format(i[0], i[1], i[2], output_info.index(i) + 2)
                    print(msg1)
        # variables with necessary info about the test
        messages_total = f'Messages total: {msg_counter}\n'
        if empty_str_counter > 0:
            empty_results = f'Empty results: {empty_str_counter} ({str((empty_str_counter / msg_counter) * 100)[:5]}%) ' \
                            f'Non-empty results: {msg_counter - empty_str_counter} ({str(100 - ((empty_str_counter / msg_counter) * 100))[:5]}%)\n'
        elif empty_str_counter == 0:
            empty_results = 'Empty results: 0%\n'
        err_print = f'Errors: {str((errors_counter / msg_counter) * 100)[:5]}%, {errors_counter} messages'

        # display this information on the screen
        print('_' * 30, '\n', end='')
        print(f'tested {test_name}', )
        print('_' * 30, '\n', end='')
        print(f'{messages_total}', end='')
        print(f'{empty_results}', end='')
        print(f'{err_print}')

    elif errors_counter == 0:
        print('No errors detected \n'
              f'Messages total: {msg_counter}\n')
    else:
        print('Something went wrong')


if __name__ == '__main__':
    # DB
    x = loader('mock_database/all_database_no_notes.csv')

    fr = FileWriter()

    # tests
    values_gettype = test_get_type(x)
    print_summary(*values_gettype)
    fr.write(*values_gettype[:-1])
