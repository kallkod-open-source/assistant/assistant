#!/usr/bin/env python
# coding: utf-8

import csv
import sys
sys.path.insert(0, '../interface_algorithm/')
from algorithm_language_func import *

algl = Algorithm_language_func(saved_model='../interface_algorithm/language_data/model.pkl')


def test():
    with open('mock_database/mock_database.csv', newline='') as f, \
         open('mock_database/output_full.csv', 'w', newline='') as fw,\
         open('mock_database/errors.csv', 'w', newline='') as fw_e:
        reader = csv.reader(f)
        writer = csv.writer(fw)
        errors_writer = csv.writer(fw_e)

        mylist = []
        next(reader)   # skip first row in input csv file
        headers = ['MESSAGE', 'EXPECTED', 'RECEIVED', 'WARNINGS']
        writer.writerow(headers)  # the column names in the output file
        errors_writer.writerow(headers[:-1])
        errors = str_counter = 0
        for row in reader:
            # row[0] - message,  row[1] - expected type
            mylist += [row[0], row[1], algl.get_type(row[0])]
            str_counter += 1
            if row[1] != algl.get_type(row[0]):
                if errors == 0:
                    print('There is an errors!\n___________________')
                errors_writer.writerow(mylist)
                print(*mylist, f' - on string {str_counter + 1}')
                mylist.append('error')
                errors += 1
            writer.writerow(mylist)
            mylist = []

        # print(errors, str_counter)
        print('percentage of errors: ', (errors/str_counter)*100)

        if errors == 0:
            return sys.exit(0)
        else:
            return sys.exit(1)


if __name__ == '__main__':
    test()
    print('lalala')
    # if exit code = 0
    # print('Ok. There is no errors')

    # if exit code = 1
    # print(f'\nDone. I found {counter} errors.')
    # print(counter, 'ошибок, всего', cntr, 'строк')
