import os
import grpc
from google.protobuf import empty_pb2

from assistant_rpc import assistant_pb2
from assistant_rpc import assistant_pb2_grpc
from assistant.interface_algorithm.helper_func import Helpers


helpers = Helpers()


class Row():
    def __init__(self, grpc_row) -> None:
        self.date       = grpc_row.date
        self.message_id = grpc_row.message_id
        self.text       = grpc_row.text
        self.deadline   = grpc_row.deadline


class AssistantClient:
    def __init__(self):
        self.channel = grpc.insecure_channel('localhost:5005')
        self.stub = assistant_pb2_grpc.AssistServerStub(self.channel)

    def close(self):
        self.channel.close()

    def get_user_list(self):
        response = self.stub.GetUserList(empty_pb2.Empty())
        users_list = [user.name for user in response.users]
        return users_list

    def create_user(self, name):
        user = assistant_pb2.User()
        user.name = name
        response = self.stub.CreateUser(user)

    def delete_user(self, username):
        path_to_user = os.path.join(helpers.user_data_path(), username)
        os.rmdir(path_to_user)

    def latest_recorded_messages(self, user, count):
        latest_msg_request = assistant_pb2.LatestMsgsRequest()
        latest_msg_request.user = user
        latest_msg_request.count = count
        response = self.stub.LatestRecordedMessages(latest_msg_request)
        
        rows = [Row(x) for x in response.rows]
        return rows, response.status

    def get_qa(self, text):
        user_input = assistant_pb2.UserInput()
        user_input.text = text
        response = self.stub.GetTypeQA(user_input)
        return response

    def process_user_input(self, user: str, text: str, count: int, deadline: str = None, forcedAction: str = None):
        DATETIME_FORMAT = 'yyyy-MM-dd HH:mm:ss'
        # packing the content in UserInput rpc message (user, text, count)
        user_input = assistant_pb2.UserInput()
        user_input.user = user
        user_input.text = text
        user_input.count = count
        
        if deadline:
            user_input.deadline = deadline.toString(DATETIME_FORMAT)

        if count < 0:
            raise Exception("count should be more than 0")

        if (forcedAction == 'READ'):
            user_input.forcedAction = assistant_pb2.UserInput.READ
        elif (forcedAction == 'WRITE'):
            user_input.forcedAction = assistant_pb2.UserInput.WRITE
        else:
            user_input.forcedAction = assistant_pb2.UserInput.AUTO

        response = self.stub.ProcessUserInput(user_input)
        rows = [Row(x) for x in response.rows]  # unpack grpc message for compatibility purposes

        action = 'READ'
        if response.action == assistant_pb2.Action.READ:
            action = 'READ'
        elif response.action == assistant_pb2.Action.WRITE:
            action = 'WRITE'
        else:
            raise Exception("response.action must be READ or WRITE")
        return rows, response.status, action
