*** Settings ***
Documentation     Test suite for assistant grpc server
Library           AssistantClient.py


*** Test Cases ***
List Of Users
    @{users} =    Get User List
    Log    ${users}
    Length Should Be    ${users}    ${3}
    Should Contain    ${users}    ferrum
    Should Contain    ${users}    ego

Create User
    Create User    Diego
    @{users} =    Get User List
    Should Contain    ${users}    Diego
    Length Should Be    ${users}    ${4}
    [Teardown]    Delete User    Diego

Get Latest Recorded Messages
    ${rows}    ${status}    Latest Recorded Messages    ferrum    ${3}
    log    ${rows}
    Length Should Be    ${rows}    ${3}
    Should be equal    ${status}    Hello, ferrum! \nShowing 3 latest recorded messages
    ${rows}    ${status}    Latest Recorded Messages    ferrum    ${4}
    log    ${rows}
    Length Should Be    ${rows}    ${4}
    Should be equal    ${status}    Hello, ferrum! \nShowing 4 latest recorded messages
    ${rows}    ${status}    Latest Recorded Messages    ferrum    ${5}
    log    ${rows}
    Length Should Be    ${rows}    ${5}
    Should be equal    ${status}    Hello, ferrum! \nShowing 5 latest recorded messages

QA Type: WRITE
    Check QA Type    I need to go to the doctor tomorrow    WRITE
    Check QA Type    I want cookies    WRITE
    Check QA Type    Red pen is on the top shelf    WRITE
    Check QA Type    Don't forget to take my passport    WRITE
    Check QA Type    pack my swimwear before the trip    WRITE

QA Type: READ
    Check QA Type    Do I need to go somewhere tomorrow?    READ
    Check QA Type    Where is that broken red pen?    READ
    Check QA Type    where's that pen?    READ
    Check QA Type    What date do I plan to travel to Italy??    READ
    Check QA Type    when will the chess club meet?    READ


Deadline Treatment: Explicit Date in Deadline 
    #                            message, string                  deadline, string
    Check Deadline In Message    Take my pills before July 5th    2022-07-05 00:00:00

    Check Deadline In Message    my brother's wedding will be on October 10    2022-10-10 00:00:00

    Check Deadline In Message    pick up the dress from the dry cleaner until December 5th    2022-12-05 00:00:00

    Check Deadline In Message    buy a Christmas garland until December 26    2022-12-26 00:00:00

    Check Deadline In Message    book hotels until February 13    2022-02-13 00:00:00

    Check Deadline In Message    I need to have time to buy curtains before March 15    2022-03-15 00:00:00


Deadline Treatment: Explicit Date And Time in Deadline
    #                            message, string                        deadline, string
    Check Deadline In Message    Take my pills before July 5th 15:00    2022-07-05 15:00:00

    Check Deadline In Message    my brother's wedding will be on October 10 15:00    2022-10-10 15:00:00

    Check Deadline In Message    pick up the dress from the dry cleaner until December 5th 15:00    2022-12-05 15:00:00

    Check Deadline In Message    buy a Christmas garland until December 26, 15:00    2022-12-26 15:00:00

    Check Deadline In Message    book hotels until February 13, 15:00    2022-02-13 15:00:00

    Check Deadline In Message    I need to have time to buy curtains before March 15, 15:00    2022-03-15 15:00:00


*** Keywords ***
Save The Message And Get Row Object
    [Documentation]    This keyword takes the message from the user, writes it in the database file
    ...                and returns single Row class object containing identical message.
    [Arguments]        ${user_message}

    ${user}=    Set Variable    ferrum
    #unpacking three values              keyword to call       user       text               count    deadline     forcedAction
    ${rows}    ${status}    ${action}    Process User Input    ${user}    ${user_message}    ${1}     ${EMPTY}     WRITE

    FOR    ${row}    IN    @{rows}
        IF    '''${row.text}''' == '''${user_message}'''    RETURN    ${row}
        Log    ${row.text}    level=DEBUG
        Log    ${user_message}    level=DEBUG
    END
    RETURN    ${-1}

Check Deadline In Message
    [Documentation]     Check how the server handles the deadline, in particular check
    ...                 the deadline from single returned Row class instance.
    ...                 -
    [Arguments]        ${user_message}    ${expected_deadline}
    #returned value     keyword                                user's message
    ${row}              Save The Message And Get Row Object    ${user_message}
    Run Keyword And Continue On Failure    Should be equal    ${row.deadline}    ${expected_deadline}
    
Check QA Type
    [Documentation]     This keyword uses get_qa method from AssistantClient library,
    ...                 which in turn call GetTypeQA from the server. This keyword allows
    ...                 simplify READ or WRITE type check, so it can be done in one line.
    ...                 
    ...                 ${response} :: rpc message ('Type')
    ...                 ${user_message} :: string, user's message
    ...                 ${read_or_write} :: string, either 'READ' or 'WRITE'
    [Arguments]        ${user_message}    ${read_or_write}
    ${response} =    Get QA    ${user_message}
    IF     '${read_or_write}' == 'READ'
        ${action}    Set Variable    ${0}
    ELSE IF    '${read_or_write}' == 'WRITE'
        ${action}    Set Variable    ${1}
    END
    Run Keyword And Continue On Failure    Should Be Equal    ${response.action}    ${action}
