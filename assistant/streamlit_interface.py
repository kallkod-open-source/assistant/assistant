#!/usr/bin/env python3
# coding: utf-8

import subprocess
import os
import streamlit as st

os.environ["TOKENIZERS_PARALLELISM"] = "false"

from assistant.interface_algorithm import algorithm_func
from assistant.interface_algorithm import assistant_func
from assistant.interface_algorithm.helper_func import user_data_path

a = algorithm_func.Algorithm_func()
assist = assistant_func.Assistant_func()
user_data_path = user_data_path()

show_input = True


# Start WEB-page
def account(data_folder=user_data_path):
    """Create 'Accounts' page, choose account or create new one, return path to user account directory"""
    st.title("Accounts")

    cna = 'create new account'
    accounts = assist.account_list(data_folder)
    accounts.append(cna)

    user_id = st.selectbox("Select account: ", accounts)  #'ego'
    
    user_dir = assist.user_existance_check(data_folder, user_id)
    # retun empty string if user doesn't exist, otherwise return path to user folder

    if user_dir:
        info = user_id + ' account has been activated successfully'
        st.success(info)
        return user_id  # 'username'

    elif not user_dir: # if user doesn't exist
        if user_id == cna: # if want to create new user
            new_user_id = st.text_input("Insert new account name?")
            new = assist.user_existance_check(data_folder, new_user_id)

        if new:
            info = st.info('Such account already exists. Please choose new account name')

        if new_user_id and not new:
            confirm = st.text_input("Are you willing to make new account? yes/no", 'no')
            if confirm == 'yes':
                user_dir = assist.create_user(data_folder, new_user_id)

                info = new_user_id + ' account has been created. Please, refresh page'
                st.success(info)
            else:
                st.error('User account creation is not confirmed.')
    return new_user_id  # 'username'


def app_assistant(username):
    dir_name = assist.user_existance_check(user_data_path, username)  # full path to '/user_data/ego/'
    title_string = "Assistant: " + username
    st.title(title_string)

    messages_number = 2;
    st.header("Latest recorded messages")
    latest_result = assist.latest_recorded_messages(dir_name, messages_number)    
    st.table(latest_result)

    message = st.text_input("Input your message here", 'hello')
    
    # save all messages from the user
    a.save_user_input(dir_name, message)
    
    todo, message_id, subject, time, is_deadline = a.message_processing(message)
    
    if show_input == True:
        st.write('Todo option:', todo)
        st.write('message_id:', message_id)
        st.write('subject:', subject)
        st.write('deadline:', is_deadline)

    if todo == 'write':
        deadline = assist.get_deadline(message, message_id)
        if deadline is not None:
            if deadline:
                st.write(deadline)
            else:
                deadline = st.text_input("Input deadline. Format YYYY-MM-DD HH:MM:")
                st.write(deadline)
        
        your_message = assist.your_message(message, message_id, deadline)
        st.header("Your message")
        st.table(your_message)
    
        st.write("If you like save message, press button")
        if st.button('Save message'):
            search_string = ''
            st.header("Registered message")
            result = a.get_access(dir_name, message, message_id, todo, search_string, assist.deadline_start_time, assist.deadline_end_time, deadline)
            st.table(result)
        
        
    if todo == 'read':
        
        # sorting resulst parameters
        column_name = 'message_id'
        ascending = True
        
        start_end_times = assist.get_access_out(message, message_id, time)
        
        if start_end_times is not None:
            start_time, end_time = start_end_times
        
            if show_input == True:
                st.write('Start time:', start_time)
                st.write('End time:', end_time)
        else:
            start_time = st.text_input("Input start of registered time. Format YYYY-MM-DD HH:MM:")
            end_time = st.text_input("Input end of registered time. Format YYYY-MM-DD HH:MM:")
    
            if show_input == True:
                st.write('Start time:', start_time)
                st.write('End time:', end_time)
            
        result = assist.subject2data(dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline)
        result = assist.sort_result_by(result, column_name, ascending)

        st.header("Results matching selected options")
        st.table(result)
        

def quit_app():
    st.write('If you like to quit Assistant, press buttom')
    if st.button('Quit Assistant'):

        st.write("Goodbye. Please close the page")
        
        output, error = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE).communicate()
        target_process = "streamlit"

        for line in output.splitlines():
            if target_process in str(line):
                pid = int(line.split(None, 1)[0])
                os.kill(pid, 9)

st.sidebar.title('Assistant navigation')
selection = st.sidebar.radio(label="", options=["Accounts", "Assistant", "Quit"])
dir_name = ''

if selection == 'Accounts':
    username = account()
    assist.write_username(username)

if selection == 'Assistant':
    username = assist.get_username()
    app_assistant(username)
        
if selection == 'Quit':
    quit_app()

