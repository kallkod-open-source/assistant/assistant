from enum import Enum

class MessageType(Enum):
    READ = 0
    WRITE = 1
