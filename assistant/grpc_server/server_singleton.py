import socket;

class ProcessGuard:
    PORT = 5001

    def lock(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM);
        try:
            self.s.bind(('localhost', ProcessGuard.PORT))
        except:
            return False

        return True
