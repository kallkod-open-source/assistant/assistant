import argparse
from concurrent import futures

import os
import logging

import grpc
from google.protobuf import empty_pb2

from assistant_rpc import assistant_pb2
from assistant_rpc import assistant_pb2_grpc

from assistant.interface_algorithm import assistant_func
from assistant.interface_algorithm import algorithm_func
from assistant.interface_algorithm import algorithm_language_func
from assistant.interface_algorithm.helper_func import Helpers
from assistant.message_type import MessageType

from assistant.grpc_server.server_singleton import ProcessGuard


class AssistantServer(assistant_pb2_grpc.AssistServerServicer):
    def __init__(self, helpers : Helpers) -> None:
        super().__init__()
        self.user_data_path = helpers.user_data_path()
        self.lang = algorithm_language_func.Algorithm_language_func(helpers)
        self.alg = algorithm_func.Algorithm_func(helpers, self.lang)
        self.assist = assistant_func.Assistant_func(helpers, self.alg)

    def GetUserList(self,  request, context):
        accounts = self.assist.account_list(self.user_data_path)
        user_list = assistant_pb2.UserList()
        user = assistant_pb2.User()
        for each_user in accounts:
            user.name = each_user
            user_list.users.append(user)
        return user_list

    def CreateUser(self,  request, context):
        new_user_id = request.name
        username = self.assist.create_user(self.user_data_path, new_user_id)
        return empty_pb2.Empty()

    def ProcessUserInput(self,  request : assistant_pb2.UserInput, context):
        user = request.user
        message = request.text # user input is a request.text
        count = request.count

        dir_name = self.assist.user_existance_check(self.user_data_path, user)
        if not dir_name:
            raise Exception('User does not exist!')

        self.alg.save_user_input(dir_name, message)

        #read custom_message_data
        custom_data = self.alg.read_custom_command(dir_name)
        custom_message = self.alg.search_data_custom_message(custom_data, message)
        if custom_message:
            message = custom_message[0]

        todo, message_id, subject, time, is_deadline = self.alg.message_processing(message)

        deadline = request.deadline or self.alg.get_deadline(message, message_id, time)
        start_time, end_time = self.alg.find_time_interval(message, message_id, time)

        if request.forcedAction == assistant_pb2.UserInput.READ:
            todo = MessageType.READ
        elif request.forcedAction == assistant_pb2.UserInput.WRITE:
            todo = MessageType.WRITE

        if message == "":
            result = self.alg.all_recorded_messages(dir_name)
            status_text = "Showing all recorded messages:"
            server_output = self.__server_output_treatment(result, status_text, assistant_pb2.Action.READ)
        else:
            if todo == MessageType.WRITE:
                server_output = self.__process_user_input_write(dir_name, 
                    message, message_id, todo, count, deadline, start_time,
                    end_time)
            if todo == MessageType.READ:
                server_output = self.__process_user_input_read(dir_name, 
                    message, message_id, todo, subject, start_time,
                    end_time, is_deadline)
        return server_output

    def __process_user_input_write(self, dir_name, message, message_id, todo, count, deadline, start_time, end_time):
        search_string = ''

        # save the message
        self.alg.get_access(dir_name, message, message_id, todo, search_string,
                                start_time, end_time, deadline)

        # return X records
        result = self.alg.latest_recorded_messages(dir_name, count)
        status_text = "Your message was saved"

        server_output = self.__server_output_treatment(result, status_text, assistant_pb2.Action.WRITE)
        return server_output

    def __process_user_input_read(self, dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline):
        result = self.assist.subject2data(dir_name, message, message_id, todo, subject, start_time, end_time, is_deadline)
        status_text = ''

        if len(result) == 0:
            status_text = "Nothing found"
        else:
            status_text = f"Search results for \"{message}\":"

        server_output = self.__server_output_treatment(result, status_text, assistant_pb2.Action.READ)
        return server_output

    def __server_output_treatment(self, result, status, action):
        output_row = assistant_pb2.OutputRow()
        server_output = assistant_pb2.ServerOutput()

        for _, row in result.iterrows():
            output_row.date = str(row.date)
            output_row.message_id = str(row.message_id)
            output_row.text = str(row.message)
            output_row.deadline = str(row.deadline)

            server_output.rows.append(output_row)
        server_output.status = status
        server_output.action = action
        return server_output

    def LatestRecordedMessages(self,  request, context):
        user = request.user #str
        count = request.count #int
        dir_name = self.assist.user_existance_check(self.user_data_path, user)
        result = self.alg.latest_recorded_messages(dir_name, count)

        status_text = f"Hello, {user}! \nShowing {count} latest recorded messages"
        server_output = self.__server_output_treatment(result, status_text, assistant_pb2.Action.READ)

        return server_output

    def GetTypeQA(self, request, context):
        text = request.text.strip()
        text_length = len(text.split())
        type_qa_rpc = assistant_pb2.Type()
        qa = self.lang.get_type_question_answer(text)
        status_text = ""

        if not text:
            type_qa_rpc.action = assistant_pb2.Action.READ
            status_text = "Press Enter key or \"Search\" to show all saved messages"

        elif text_length == 1 or qa == MessageType.READ:
            type_qa_rpc.action = assistant_pb2.Action.READ
            status_text = "Press Enter key or \"Search\" button to search for the entered text."

        elif qa == MessageType.WRITE:
            type_qa_rpc.action = assistant_pb2.Action.WRITE
            status_text = "Your message will be saved.\nRecently saved messages:"

        else:
            raise Exception("qa should be either READ or WRITE")

        type_qa_rpc.status = status_text
        return type_qa_rpc


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--data-dir", help="Data directory")
    args = parser.parse_args()

    logging.basicConfig()
    logger = logging.getLogger('server')

    guard = ProcessGuard()
    if not guard.lock():
        logger.critical('Another server process is running already')
        os.sys.exit(1)

    helpers = Helpers(args.data_dir)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=5))
    assistant_pb2_grpc.add_AssistServerServicer_to_server(
        AssistantServer(helpers), server)
    server.add_insecure_port('127.0.0.1:5005')
    server.start()
    server.wait_for_termination()
