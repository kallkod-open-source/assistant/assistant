# Git configuration

Please apply following configuration to each repository belonging to Källkod:

```bash
git config user.name  "FirstName LastName"
git config user.email "<firstname.lastname@kallkod.fi>"
```
You either need to run it after each `git clone` or set it as a global configuration.

# Instructions for installing the Assistant in a virtual environment as a package. 
Dependencies are adapted for **Python 3.8.10**.



```bash
cd ~/Work/
git clone git@gitlab.com:kallkod-assistant/assistant.git # clone git repository

sudo apt install python3.8-venv # Install python3.8-venv

python3 -m venv ~/Work/virtualenvs/env # Create new virtual environment. 
source ~/Work/virtualenvs/env/bin/activate # activate it
pip3 install wheel
pip3 install -U pip # update pip to latest version:

cd ~/Work/assistant/    # this is root project folder
pip install -e . # Install Assistant package to virtual environment. Dependencies should be installed automatically.
```
This way it is installed in an editable state, and all the edits made to the .py files will be automatically included in the installed package.<br>

If the installation of dependencies fails for example due to a broken internet connection, clearing the cache will help. This command will clean pip's cache (in unix this is folder `~/.cache/pip` which could be used to store downloaded packages by _all_ the virtrual environments you have, so _be careful!_):

`pip cache purge`


## How to run Assistant with streamlit interface:
```bash
# 1) activate virtual environment
source ~/Work/virtualenvs/env/bin/activate

# 2) cd to directory containing streamlit_interface.py file
cd ~/Work/assistant/assistant/

# 3) run following command:
./assistant
# or:
streamlit run streamlit_interface.py
```


## How to run gRPC server:
```bash
# 1) activate virtual environment
source ~/Work/virtualenvs/env/bin/activate

# 2) cd to grpc_server directory
cd ~/Work/assistant/assistant/grpc_server/

# 3) run server:
python3 server.py
```
! After you launched the server, there is no output, it's okay. It means that server is launched.

## Version numbers
Version numbers in `setup.py` should follow https://peps.python.org/pep-0440/ . GIT tags should follow https://semver.org/
Examples to follow:

| Type        | git tag | setup.py version |
|------       |---------|------------------|
| Release     | 1.0.0   | 1.0.0            |
| Development | 1.1.0-1 | 1.0.0.dev1       |
